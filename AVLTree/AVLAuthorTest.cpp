/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

// Struct goes here
struct Author{

  std::string FirstName;
  std::string LastName;

  Author(std::string FirstName, std::string LastName) : FirstName(FirstName), LastName(LastName) {}


/********************************************
* Function Name  : operator<
* Pre-conditions : const Author& rhs
* Post-conditions: bool
*
* This function overloads the < operator
********************************************/

  bool operator<(const Author& rhs) const{
 
    //lhs < rhs
    if (LastName < rhs.LastName)
      return true;
    else if (LastName == rhs.LastName) {
      if (FirstName < rhs.FirstName) {
        return true;
      }
    }        
 
    return false;
  }

  /********************************************
* Function Name  : operator==
* Pre-conditions : const Author& rhs
* Post-conditions: bool
*
* This function overloads the == operator
********************************************/

  bool operator==(const Author& rhs) const{
    if(LastName != rhs.LastName)
      return false;
    else {
      if (FirstName != rhs.FirstName)
        return false;
    }
    return true;

  }
  
  friend std::ostream& operator<<(std::ostream& outStream, const Author& printAuth);

};


/********************************************
* Function Name  : operator<<
* Pre-conditions : std::ostream& outStream, const Author& printAuth
* Post-conditions: std::ostream&
*
* This function overloads the << operator
********************************************/

std::ostream& operator<<(std::ostream& outStream, const Author& printAuth) {

  outStream << printAuth.LastName << ", " << printAuth.FirstName;

  return outStream;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    //create initial AVLTree
    AVLTree<Author> authorsAVL;

    Author Aardvark("Anthony", "Ardvark"); 
    Author Aardvark2("Gregory", "Aardvark");
    Author BadPerson ("BadMean", "Person");   	
    Author Main("Michael","Main");
    Author McDowell("Gayle","McDowell");
    Author Savitch("Walter", "Savitch");

    authorsAVL.insert(Aardvark);   
    authorsAVL.insert(Aardvark2);
    authorsAVL.insert(BadPerson);
    authorsAVL.insert(Savitch);
    authorsAVL.insert(Main);
    authorsAVL.insert(McDowell);

    authorsAVL.printTree();

    std::cout << "removing " << BadPerson << std::endl;

    authorsAVL.remove(BadPerson);

    authorsAVL.printTree(); 
    return 0;
}
