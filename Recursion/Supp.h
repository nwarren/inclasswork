#ifndef SUPP_H
#define SUPP_H

#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <cstdlib>		// exitr

double getArgv1Num(int argc, char** argv){
	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}
	
	// stringstream used for the conversion initialized with the contents of argv[1]
	double factNum;
	std::stringstream convert(argv[1]);

	//give the value to factNum using the characters in the string
	if ( !(convert >> factNum) ){
		std::cout << "Not a valid integer" << std::endl;
		exit(-1);
	}
	
	return factNum;
	
}

#endif
