/*****************************************
 * Filename: Section01_DynArrayTemplate.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file demonstrates the purpose of dynamic 
 * memory allocation using templates
 * 
 * array_test tests the copy constructor, assignment operator
 * and destructor of the templated class.
 * 
 * Requires Section01_Array.h to run 
 * **************************************/

#include <iostream> // for cout 
#include <cstdlib> // for rand()
#include <ctime> // for time()
#include <cassert> // for assert()
#include "Section01_DynArray.h"

const int NITEMS = 1<<10;
const int TRIALS = 100;
 
 /******************************
 * Function Name: find
 * Preconditions: InputIterator first, InputIterator last, const T& 
 * Postconditions: InputIterator
 * 
 * Iterates from the first to the last element. If a value
 * is found, it returns the iterator of that element
 * Otherwise, it will return the last iterator
 * ****************************/
template<class InputIterator, class T>
InputIterator find (InputIterator first, InputIterator last, const T& val)
{
    while (first!=last) {
      if (*first==val) 
        return first;
      ++first;
    }
    return last;
}

/******************************
 * Function Name: duplicates 
 * Preconditions: int n 
 * Postconditions: bool 
 * This function tests the array by creating a list, and
 * then iterating to find duplicates 
 * ****************************/
bool duplicates(int n) {
    Array<int> randoms(n);

    for (int i = 0; i < NITEMS; i++) {
    	randoms[i] = rand() % 1000;
    }

    for (int i = 0; i < n; i++) {
        int* iter;
    	if ((iter = find(randoms.begin(), randoms.end(), randoms[i])) != randoms.end()) {
    	    std::cout << *iter << " and " << randoms[i] << ": ";
    	    return true;
    	}
    }

    return false;
}

/******************************
 * Function Name: array_test
 * Preconditions: none 
 * Postconditions: none 
 * This function creates an array, and then
 * tests the copy constructor, assignment operator
 * and the swap functions.
 * ****************************/
void array_test() {
    Array<int> a0;

    for (int i = 0; i < NITEMS; i++) {
    	a0.push_back(rand() % 1000);
    }

    Array<int> a1(a0);
    for (int i = 0; i < NITEMS; i++) {
    	assert(a0[i] == a1[i]);
    }

    Array<int> a2;
    a2 = a0;
    for (int i = 0; i < NITEMS; i++) {
    	assert(a0[i] == a2[i]);
    }
    
    Array<int> a3;
    a3.swap(a1);
    for (int i = 0; i < NITEMS; i++) {
    	assert(a0[i] == a3[i]);
    }
}

/************************************
 * Function name: main
 * Preconditions: int, char **
 * Postconditions: int 
 * 
 * This is the main driver function
 * *********************************/
int main(int argc, char**argv)
{
    // Initialize the double array pointers
    srand(time(NULL));

    for (int i = 0; i < TRIALS; i++) {
    	if (duplicates(NITEMS)) {
    	    std::cout << "Duplicates Detected!" << std::endl;
    	}
    }

    array_test();

	return 0;
}