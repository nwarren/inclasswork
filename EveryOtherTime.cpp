/**********************************************
* File: EveryOtherTime.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is a test file 
**********************************************/
#include <iostream>

int main(int argc, char** argv){

	std::cout << "Every other time" << std::endl;

	return 0;

}

